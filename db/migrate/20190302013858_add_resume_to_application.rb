class AddResumeToApplication < ActiveRecord::Migration[5.2]
  def up
    add_attachment :applications, :resume
  end
end
