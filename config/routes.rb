Rails.application.routes.draw do
  apipie
  mount_devise_token_auth_for 'User', at: 'auth'

  scope '/v:api_version', module: "v1", as: :v1, defaults: { api_version: '1' } do
    # POST
    resources :posts, only: [:index, :show, :create, :update, :destroy]
    # APPLICATION
    resources :applications, only: [:index, :show, :create, :update, :destroy] do
      member do
        patch :upload_resume
      end
    end
  end
end
