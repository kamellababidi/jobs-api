class PostPolicy < BasePolicy

  # anyone can list all posts
  def index?
    true
  end

  # anyone can see post
  def show?
    true
  end

  # only admin user can create post
  def create?
    is_admin?
  end

  # only admin can edit post
  def update?
    is_admin?
  end

  # only admin can delete post
  def destroy?
    is_admin?
  end

  private

  def is_admin?
    @user.admin?
  end


end
