class ApplicationPolicy < BasePolicy

  attr_reader :user, :scope

  def initialize(user, scope)
    @user  = user
    @scope = scope
  end

  # only admin can list all applications
  def index?
    is_admin?
  end

  # the owner can see his application or admin
  def show?
    is_owner? || is_admin?
  end

  # only seeker can create application
  def create?
    is_seeker?
  end

  # only seeker can update his application
  def update?
    is_seeker? && is_owner?
  end

  # only seeker can delete his application
  def destroy?
    is_seeker? && is_owner?
  end

  private

  def is_admin?
    @user.admin?
  end

  def is_seeker?
    @user.user?
  end

  def is_owner?
    @user.id == scope.user_id
  end

end
