class UserNotifierMailer < ApplicationMailer
  default :from => 'kamelallababidi@gmail.com'

  def send_seen_email(user)
    @user = user
    mail( :to => @user.email,
          :subject => 'Your Application has seen by admin' )
  end
end
