class V1::BaseController < ApplicationController
  before_action :authenticate_user!

  rescue_from ActiveRecord::RecordInvalid, with: :render_validation_error
  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  rescue_from Pundit::NotAuthorizedError, with: :not_authorized

  private

  def render_validation_error(error)
    render json: {errors: error.record.errors.messages}, status: :unprocessable_entity
  end

  def not_found
    render json: { errors: I18n.t('errors.not_found') }, status: :not_found
  end

  def not_authorized
    render json: { errors: I18n.t('errors.not_autorized') }, status: :forbidden
  end
end