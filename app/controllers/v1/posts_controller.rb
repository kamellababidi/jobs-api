module V1
  class PostsController < BaseController

    before_action :set_post, only: [:show, :update, :destroy]
    before_action -> { authorize Post }

    api :GET, '/v1/posts?title=dad', "Get all posts or search by title"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    example '
    Response:
    [
      {
          "posts": {
              "id": 2,
              "title": "sdad",
              "description": "1"
          }
      },
      {
          "posts": {
              "id": 3,
              "title": "sdad",
              "description": "sdasd"
          }
      }
    ]
    '
    def index
      render json: filter_result, root: :posts
    end

    api :GET, '/v1/posts/:id', "Show single post by ID"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    example '
    Response:
    {
      "post": {
          "id": 6,
          "title": "software engineer",
          "description": "lab lab ..."
      }
    }
    '
    def show
      render json: @post, root: :post
    end

    api :POST, '/v1/posts', "Create new job post"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param :title, String, desc: "BODY param: title of job post", required: true
    param :description, String, desc: "BODY param: description of job post", required: true
    def create
      Post.create!(permit_params)
    end

    api :PUT, '/v1/posts/:id', "Update specific job post"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param :title, String, desc: "BODY param: title of job post", required: false
    param :description, String, desc: "BODY param: description of job post", required: false
    def update
      @post.update!(permit_params)
    end

    api :DELETE, '/v1/posts/:id', "delete specific job post"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    def destroy
      @post.destroy!
    end

    private

    def permit_params
      params.require(:post).permit(:title, :description)
    end

    def set_post
      @post = Post.find(params[:id])
    end

    def filter_result
      result = []
      if params[:title].present?
        result = Post.where("title ILIKE ?", "%#{params[:title]}%")
      else
        result = Post.all
      end
      result
    end

  end
end
