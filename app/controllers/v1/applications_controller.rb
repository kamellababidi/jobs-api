module V1
  class ApplicationsController < BaseController

    before_action :set_application, only: [:show, :update, :destroy, :upload_resume]

    api :GET, '/v1/applications', "List all job applications"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    def index
      authorize Application
      render json: Application.all, root: :applications
    end

    api :GET, '/v1/applications/:id', "Show specific job application"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    def show
      authorize @application
      seen
      # UserNotifier.send_seen_email(@user).deliver  #needs #more configurations#
      render json: @application, root: :application
    end

    api :POST, '/v1/applications', "Create new job application"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param :user_id, String, desc: "BODY param: your user id", required: true
    param :description, String, desc: "BODY param: the post id you to apply for it", required: true
    def create
      authorize Application
      Application.create!(permit_params)
    end

    api :PUT, '/v1/applications/:id', "Update specific job application"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param :user_id, String, desc: "BODY param: your user id", required: false
    param :description, String, desc: "BODY param: the post id you to apply for it", required: false
    def update
      authorize @application
      @application.update!(permit_params)
    end

    api :DELETE, '/v1/applications/:id', "Delete specific job application"
    param 'access-token <access-token>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'token-type <token-type>', String, :desc => "HEADER param: access-token you get from sign-in", :required => false
    param 'client <client>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    param 'uid <uid>', String, :desc => "HEADER param: your email", :required => true
    param 'expiry <expiry>', String, :desc => "HEADER param: access-token you get from sign-in", :required => true
    def destroy
      authorize @application
      @application.destroy!
    end

    def upload_resume
      @application.update!(resume: params[:resume])
    end

    private

    def permit_params
      params.require(:application).permit(:user_id, :post_id, :resume)
    end

    def set_application
      @application = Application.find(params[:id])
    end

    def seen
      @application.update(seen: true) if current_user.admin? && !@application.seen
    end

  end
end
