class Users::SessionsController < DeviseTokenAuth::SessionsController

  api :Post, '/auth/sign_in', "Login user (Signin)"
  param :email, String, desc: "BODY param: your email", required: true
  param :password, String, desc: "BODY param: your password (8 digits)", required: true
  example '{
    "data": {
        "id": 1,
        "email": "kamel@gmail.com",
        "provider": "email",
        "uid": "kamel@gmail.com",
        "allow_password_change": false,
        "name": null,
        "nickname": null,
        "image": null,
        "role": "admin"
    }
  }'
  example 'HEADER: ****
    access-token →bzJbuHAnrQ5sMYEKHMrzPA
    token-type →Bearer
    client →pH9D-ECbgzjqA0CLxDHupw
    expiry →1552409536
    uid →kamel@mailinator.com
    Content-Type →application/json; charset=utf-8
    ETag →W/"623ba742a3eb153362704c79ff1d144a"
    Cache-Control →max-age=0, private, must-revalidate
    X-Request-Id →84c9c2dc-95ff-4fae-8b85-169a61d72f48
    X-Runtime →0.369040
    Transfer-Encoding →chunked'
  def create
    super
  end

end