class Application < ActiveRecord::Base

  # relations
  belongs_to :user
  belongs_to :post
  has_attached_file :resume, default_url: "/resume/resume.pdf"


  # validations
  validates_attachment_content_type :resume, content_type: ["application/pdf"]
  validates :user_id, :post_id, presence: true
  validates :post_id, uniqueness: {message:I18n.t('application.unique')}

end
