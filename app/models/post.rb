class Post < ActiveRecord::Base

  # relations
  has_many :applications
  has_many :users, through: :applications

  # validations
  validates :title, :description, presence: true

end
